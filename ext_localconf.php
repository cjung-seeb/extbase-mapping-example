<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Jodimas.' . $_EXTKEY,
	'Mappingexample',
	array(
		'Item' => 'list, show, new, create, edit, update, delete',
		
	),
	// non-cacheable actions
	array(
		'Item' => 'list, create, update, delete',
		
	)
);

?>