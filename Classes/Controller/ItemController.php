<?php
namespace Jodimas\Mappingexample\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mappingexample
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ItemController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * itemRepository
	 *
	 * @var \Jodimas\Mappingexample\Domain\Repository\ItemRepository
	 * @inject
	 */
	protected $itemRepository;

    /**
     * pageRepository
     *
     * @var \Jodimas\Mappingexample\Domain\Repository\PageRepository
     * @inject
     */
    protected $pageRepository;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$items = $this->itemRepository->findAll();
		$this->view->assign('items', $items);

        $pages = $this->pageRepository->fetchAll()->getFirst();
        \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($pages, 'Pages');
        $this->view->assign('pages', $pages);
	}

	/**
	 * action show
	 *
	 * @param \Jodimas\Mappingexample\Domain\Model\Item $item
	 * @return void
	 */
	public function showAction(\Jodimas\Mappingexample\Domain\Model\Item $item) {
		$this->view->assign('item', $item);
	}

	/**
	 * action new
	 *
	 * @param \Jodimas\Mappingexample\Domain\Model\Item $newItem
	 * @dontvalidate $newItem
	 * @return void
	 */
	public function newAction(\Jodimas\Mappingexample\Domain\Model\Item $newItem = NULL) {
		$this->view->assign('newItem', $newItem);
	}

	/**
	 * action create
	 *
	 * @param \Jodimas\Mappingexample\Domain\Model\Item $newItem
	 * @return void
	 */
	public function createAction(\Jodimas\Mappingexample\Domain\Model\Item $newItem) {
		$this->itemRepository->add($newItem);
		$this->flashMessageContainer->add('Your new Item was created.');
		$this->redirect('list');
	}

	/**
	 * action edit
	 *
	 * @param \Jodimas\Mappingexample\Domain\Model\Item $item
	 * @return void
	 */
	public function editAction(\Jodimas\Mappingexample\Domain\Model\Item $item) {
		$this->view->assign('item', $item);
	}

	/**
	 * action update
	 *
	 * @param \Jodimas\Mappingexample\Domain\Model\Item $item
	 * @return void
	 */
	public function updateAction(\Jodimas\Mappingexample\Domain\Model\Item $item) {
		$this->itemRepository->update($item);
		$this->flashMessageContainer->add('Your Item was updated.');
		$this->redirect('list');
	}

	/**
	 * action delete
	 *
	 * @param \Jodimas\Mappingexample\Domain\Model\Item $item
	 * @return void
	 */
	public function deleteAction(\Jodimas\Mappingexample\Domain\Model\Item $item) {
		$this->itemRepository->remove($item);
		$this->flashMessageContainer->add('Your Item was removed.');
		$this->redirect('list');
	}

}
?>